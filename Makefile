data: ucd.all.flat.xml allkeys.txt

ucd.all.flat.xml: ucd.all.flat.zip
	unzip ucd.all.flat.zip

ucd.all.flat.zip:
	wget https://www.unicode.org/Public/11.0.0/ucdxml/ucd.all.flat.zip

allkeys.txt:
	wget https://www.unicode.org/Public/UCA/11.0.0/allkeys.txt

